from tkinter import *
from tkinter import ttk
from tkinter import simpledialog
import tkinter.messagebox
import matplotlib
matplotlib.use("TkAgg")
from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg
from matplotlib.figure import Figure
import matplotlib.pyplot as plt
import os
import pandas as pd
import numpy as np
import datetime
from random import randint
import pickle

from utils import ReadCorrections


class MyWindow:

    def __init__(self, master, dataset, **kwargs):
        master.title("Time-series correctness assessment")
        self.master = master
        self.datafile = []
        self.master.columnconfigure(0, weight=1)
        self.master.rowconfigure(0, weight=1)
        self.master.rowconfigure(1, weight=5)
        self.master.rowconfigure(2, weight=5)
        self.master.rowconfigure(3, weight=5)
        self.master.rowconfigure(4, weight=5)
        self.master.rowconfigure(5, weight=5)
        self.master.rowconfigure(6, weight=5)
        self.master.rowconfigure(7, weight=5)
        self.master.rowconfigure(8, weight=5)
        self.master.rowconfigure(9, weight=5)

        # self.master.geometry("1200x700")
        self.master.configure(background="white")

        # Setting the counter to its starting position
        self.counter = 0
        self.counterPrev = 0

        # Read csv data
        self.my_data = dataset

        # Read the list of indices for 5 nearest neighbours
        # if 'lage' in datafile:
        #     with open("neighbours_m_lage.bin", "rb") as file:
        #         self.NNindices = pickle.load(file)
        #     with open("distances_m_lage.bin", "rb") as file:
        #         self.NNdists = pickle.load(file)
        # elif 'hoge' in datafile:
        #     with open("neighbours_m_hoge.bin", "rb") as file:
        #         self.NNindices = pickle.load(file)
        #     with open("distances_m_hoge.bin", "rb") as file:
        #         self.NNdists = pickle.load(file)

        # ***** Some initializations *****
        self.fig = Figure()
        self.ax = self.fig.add_subplot(111)
        self.ax2 = self.ax.twiny()
        self.fig.set_tight_layout(True)
        
        self.ansP = {}
        self.ansS = {}
        self.corrections = None
        if kwargs:
            self.corrections = ReadCorrections(kwargs['file'])

        self.segmentation = {}
        self.points = {}

        self.shift = {}
        self.shift_counter = 1

        # Initializations of StringVars and IntVars
        self.checked = IntVar(master=self.master)
        self.checked.set(0)
        self.checked_rand = IntVar(master=self.master)
        self.checked_rand.set(0)
        self.show_model = IntVar(master=self.master)
        self.show_model.set(1)
        
        self.labelmode = StringVar(master=self.master)
        self.labelmode.set("lines")

        self.title_var = StringVar(master=self.master)
        self.title_var.set("Time-series correctness assessment\n" + self.my_data.area_name + " - time-series #" + str(self.counter))

        self.model = StringVar(master=self.master)
        if self.my_data.fitted_models:
            self.model.set(self.my_data.fitted_models[self.counter])
        else:
            self.model.set("empty")

        self.point_info = StringVar(master=self.master)
        self.point_info.set(str(self.my_data.point_id.iloc[self.counter]) + "\n"
                            + "%0.1f" % (self.my_data.linear.iloc[self.counter] * 1000) + "\n"
                            + "%0.5f" % (self.my_data.height.iloc[self.counter]) + "\n"
                            + "%0.6f" % (self.my_data.dem_height.iloc[self.counter]) + "\n"
                            + str(self.my_data.data.shape[1]))
        
        self.end_x = (self.my_data.num_of_obs_full - 1) * self.my_data.revisit / 365.25
        
        xx = np.linspace(0, self.end_x, int(self.my_data.num_of_obs_full))
        full_dates = pd.date_range(start=self.my_data.data.columns[0],
                                   end=self.my_data.data.columns[-1], freq=str(int(self.my_data.revisit)) + 'D')
        all_x = pd.DataFrame(xx, index=full_dates, columns=['x'])
        orig_x_val = all_x.reindex(self.my_data.data.columns).values.reshape((int(self.my_data.num_of_act_obs),))
        self.x_orig = orig_x_val[:]
        self.y_orig = self.my_data.data.iloc[self.counter, :].T.values * 1000

        self.make_widgets()

    def make_widgets(self):

        label_title1 = Label(self.master, textvariable=self.title_var, bg="white", font="Verdana 12")
        label_title1.grid(row=0, column=0, columnspan=2, sticky=N)
        label_title1.grid_rowconfigure(0, weight=0, minsize=200)
        label_title1.grid_columnconfigure(0, weight=1)

        label_info = LabelFrame(self.master, text="Point Info", bg="white", font="Verdana 12")
        label_info.grid(row=1, column=1, padx=25, pady=5, sticky=N)
        label_info.grid_rowconfigure(1, weight=1)

        point_info = Label(label_info, text="Point:\nLinear:\nHeight:\nDEM Height:\nNr. of Obsv.:",
                           bg="white", justify=LEFT)
        point_info.pack(side=LEFT)

        point_info2 = Label(label_info, textvariable=self.point_info, bg="white", justify=LEFT)
        point_info2.pack(side=RIGHT)

        s = ttk.Style(master=self.master)
        s.configure("Accept.TButton", foreground="green", activebackground="yellowgreen", font="TkDefaultFont 16 bold")
        s.map("Accept.TButton", background=[("active", "yellowgreen")])
        button1 = ttk.Button(self.master, text="It's fine", style="Accept.TButton", command=self.AcceptButton)
        button1.grid(row=2, column=1, padx=10, pady=5, ipadx=15, ipady=10, sticky=N)
        button1.grid_rowconfigure(2, weight=1)

        s.configure("DontAccept.TButton", foreground="darkred", activebackground="red", font="TkDefaultFont 16 bold")
        s.map("DontAccept.TButton", background=[("active", "red")])
        button2 = ttk.Button(self.master, text="Unwrapping", style="DontAccept.TButton",
                             command=self.UnwrappingButton)
        button2.grid(row=3, column=1, padx=10, pady=5, ipadx=15, ipady=10, sticky=N)
        button2.grid_rowconfigure(3, weight=1)

        s.configure("DontAccept.TButton", foreground="darkred", activebackground="red", font="TkDefaultFont 16 bold")
        s.map("DontAccept.TButton", background=[("active", "red")])
        button3 = ttk.Button(self.master, text="Model", style="DontAccept.TButton",
                             command=self.ModelButton)
        button3.grid(row=4, column=1, padx=10, pady=5, ipadx=15, ipady=10, sticky=N)
        button3.grid_rowconfigure(4, weight=1)

        s.configure("DontAccept.TButton", foreground="darkred", activebackground="red", font="TkDefaultFont 16 bold")
        s.map("DontAccept.TButton", background=[("active", "red")])
        button4 = ttk.Button(self.master, text="Both", style="DontAccept.TButton",
                             command=self.BothButton)
        button4.grid(row=5, column=1, padx=10, pady=5, ipadx=15, ipady=10, sticky=N)
        button4.grid_rowconfigure(5, weight=1)

        # button5 = ttk.Button(self.master, text="Add comment", style="My.TButton",
        #                      command=self.AddComment)
        # button5.grid(row=6, column=1, padx=10, pady=5, ipadx=15, ipady=10, sticky=S)
        # button5.grid_rowconfigure(6, weight=1)

        button5 = ttk.Button(self.master, text="Refresh plot", style="My.TButton",
                             command=self.PointUpdate)
        button5.grid(row=6, column=1, padx=10, pady=5, ipadx=15, ipady=10, sticky=S)
        button5.grid_rowconfigure(6, weight=1)

        button6 = ttk.Button(self.master, text="Show neighbours", style="My.TButton",
                             command=self.NNPlotsBox)
        button6.grid(row=7, column=1, padx=10, pady=0, ipadx=15, ipady=10, sticky=S)
        button6.grid_rowconfigure(7, weight=1)

        s.configure("My.TCheckbutton", background="white")
        check = ttk.Checkbutton(self.master, text="Go to the next one automatically", variable=self.checked,
                                style="My.TCheckbutton")
        check.grid(row=8, column=1, padx=10, sticky=S)
        
        check_randomize = ttk.Checkbutton(self.master, text="Shuffle points randomly", variable=self.checked_rand,
                                style="My.TCheckbutton")
        check_randomize.grid(row=8, column=1, pady=(15,20), sticky=S)

        check_randomize = ttk.Checkbutton(self.master, text="The model line is visible", variable=self.show_model,
                                style="My.TCheckbutton")
        check_randomize.grid(row=8, column=1, pady=(15,60), sticky=S)

        s.configure("My.TButton", font="TkDefaultFont 12")
        nextButton = ttk.Button(self.master, text="Next    >", style="My.TButton", command=self.NextButton)
        nextButton.grid(row=9, column=1, padx=(100,0), pady=15)
        nextButton.grid_rowconfigure(9, weight=1)

        finishButton = ttk.Button(self.master, text="Finish", style="My.TButton", command=self.FinishButton)
        finishButton.grid(row=9, padx=15, pady=15, sticky=W)
        finishButton.grid_rowconfigure(9, weight=1)

        prevButton = ttk.Button(self.master, text="Previous", style="My.TButton", command=self.PreviousButton)
        prevButton.grid(row=9, padx=15, pady=15, sticky=E)
        prevButton.grid_rowconfigure(9, weight=1)
        
        backButton = ttk.Button(self.master, text="<    Back", style="My.TButton", command=self.BackButton)
        backButton.grid(row=9, column=1, padx=(0,150), pady=15, sticky=E)
        backButton.grid_rowconfigure(9, weight=1)

        changeButton = ttk.Button(self.master, text="Go to...", style="My.TButton", command=self.ChangeButton)
        changeButton.config(width = 8)
        changeButton.grid(row=9, padx=(15,130), ipadx=0, pady=15, sticky=E)
        changeButton.grid_rowconfigure(9, weight=1)
        
        clearButton = ttk.Button(self.master, text="Clear segmentation", style="My.TButton", command=self.ClearButton)
        clearButton.grid(row=9, padx=(15,220), pady=15, sticky=E)
        clearButton.grid_rowconfigure(9, weight=1)
        
        clearPointsButton = ttk.Button(self.master, text="Clear points", style="My.TButton", command=self.ClearPointsButton)
        clearPointsButton.grid(row=9, padx=(15,390), pady=15, sticky=E)
        clearPointsButton.grid_rowconfigure(9, weight=1)
        
        s.configure("radio.TCheckbutton", background="white", font="TkDefaultFont 12")
        segmentationButton = ttk.Radiobutton(self.master, text="Segmentation", style="radio.TCheckbutton", 
                                             variable=self.labelmode, value="lines")
        segmentationButton.grid(row=9, padx=(15,510), pady=15, sticky=E)
        segmentationButton.grid_rowconfigure(9, weight=1)
        
        pointsButton = ttk.Radiobutton(self.master, text="Points", style="radio.TCheckbutton", 
                                       variable=self.labelmode, value="points")
        pointsButton.grid(row=9, padx=(15,640), pady=15, sticky=E)
        pointsButton.grid_rowconfigure(9, weight=1)
        
        # shiftButton = ttk.Radiobutton(self.master, text="Shift", style="radio.TCheckbutton",
        #                                variable=self.labelmode, value="shift")
        # shiftButton.grid(row=9, padx=(15,725), pady=15, sticky=E)
        # shiftButton.grid_rowconfigure(9, weight=1)

        
        self.MakeAPlot()

        plotFrame = Frame(self.master)
        plotFrame.grid(row=1, column=0, columnspan=1, rowspan=8, sticky=N + S +E + W)
        plotFrame.grid_columnconfigure(0, weight=5)

        self.canvas = FigureCanvasTkAgg(self.fig, plotFrame)
        self.canvas.draw()
        self.canvas.get_tk_widget().pack(side=TOP, fill=BOTH, expand=True)
        

        self.canvas.mpl_connect('button_press_event', self.OnClick)
        

    def MakeAPlot(self):

        # ***** Figure plotting *****
        self.ax.set_xlabel("Year")
        self.ax.set_ylabel("Deformation [mm]")
        self.ax.grid(True, color="lightgrey")
        self.ax.set_axisbelow(True)
        if self.corrections is not None and self.counter in self.corrections.keys():
            temp_series = self.my_data.data.iloc[self.counter, :].T.copy()
            for i, idx in enumerate(self.corrections[self.counter]['corr_idx']):
                temp_series[idx] = -1 * self.corrections[self.counter]['sign'][i] * self.my_data.pi_shift/1000 + temp_series[idx]
            self.ax.plot(self.my_data.data.columns, self.my_data.data.iloc[self.counter, :].T * 1000, '--o',
                         markersize=3.5, linewidth=1, color='grey', markerfacecolor='red', markeredgecolor='red')
            self.ax.plot(self.my_data.data.columns, temp_series * 1000, '-o',
                         markersize=3.5, linewidth=1, color='grey', markerfacecolor='C0', markeredgecolor='C0')

        else:
            self.ax.plot(self.my_data.data.columns, self.my_data.data.iloc[self.counter, :].T * 1000, '-o',
                         markersize=3.5, linewidth=1, color='grey', markerfacecolor='C0', markeredgecolor='C0')

        self.ax.scatter(self.my_data.data.columns, self.my_data.data.iloc[self.counter, :].T * 1000 + self.my_data.pi_shift, s=10,
                        c="wheat")
        self.ax.scatter(self.my_data.data.columns, self.my_data.data.iloc[self.counter, :].T * 1000 - self.my_data.pi_shift, s=10,
                        c="lightblue")

        if (self.my_data.data.iloc[self.counter, :].max() * 1000 < 5):
            self.ax.set_ylim(top=25)
        else:
            self.ax.set_ylim(top=self.my_data.data.iloc[self.counter, :].max() * 1000 + 20)
        if (self.my_data.data.iloc[self.counter, :].min() * 1000 > -10):
            self.ax.set_ylim(bottom=-40)
        else:
            self.ax.set_ylim(bottom=self.my_data.data.iloc[self.counter, :].min() * 1000 - 20)

        # plotting the fitted model
        self.ax2.axhline(y=0, xmin=0, xmax=1, c='black', lw=1)
        x = np.linspace(0, self.end_x, int(self.my_data.num_of_obs_full))
        if self.model.get() != "empty":
            if self.show_model.get():
                eval('self.ax2.plot(x,1000*(' + self.model.get()+'), color="blueviolet")')
            else:
                eval('self.ax2.plot(x,1000*(' + self.model.get() + '), color="None")')
        else:
            self.ax2.plot(x, [0]*len(x), color="None")
        self.ax2.tick_params(axis='x', top=False, labeltop=False)

        # plotting the segmentation lines
        if self.counter in self.segmentation.keys():
            for item in self.segmentation[self.counter]:
                # print(self.segmentation, item)
                self.ax2.axvline(x=item[0], linestyle='--', c='k')
                # self.ax2.axvline(x=self.x_orig[item[1]], linestyle='--', c='g')

        # plotting the selected points
        self.y_orig = self.my_data.data.iloc[self.counter, :].T.values * 1000
        if self.counter in self.points.keys():
            for item in self.points[self.counter]:
                self.ax2.scatter(x=item[0], y =item[1], s=14, facecolors="none", edgecolors="grey")
                self.ax2.scatter(x=self.x_orig[item[2]], y=self.y_orig[item[2]], s=14, facecolors="red", edgecolors="red")
                
        # plotting the selected shifts
        if self.counter in self.shift.keys():
            for item in self.shift[self.counter]:
                self.ax2.scatter(x=item[0], y =item[1], s=14, facecolors="none", edgecolors="grey")
                self.ax2.scatter(x=self.x_orig[item[2]], y=self.y_orig[item[2]], s=14, facecolors="red", edgecolors="red")
                self.ax2.axvline(x=self.x_orig[item[2]], linestyle='--', c='red')
                
        if self.ansP:
            if self.counter in self.ansP.keys():
                for item in self.ansP[self.counter]:
                    self.ax2.scatter(x=self.x_orig[item], y=self.y_orig[item], s=14, facecolors="orange", edgecolors="red")
                for item in self.ansS[self.counter]:
                    print(item)
                    self.ax2.axvline(x=self.x_orig[item], linestyle='--', c='darkgreen')

    def MakeSubPlots(self):

        axs = [self.ax_1, self.ax_2, self.ax_3, self.ax_4, self.ax_5, self.ax_6]
        axms = [self.axm_1, self.axm_2, self.axm_3, self.axm_4, self.axm_5, self.axm_6]

        titles = ['CURRENT time series #{}'.format(self.counter)]
        for i in range(1,6):
            tmp_title = "Time series #{}".format(self.NNindices[self.counter][i]) + "  (~ " + \
                        "{:.1f}".format(np.abs(self.NNdists[self.counter][i])) + " m)"
            titles.append(tmp_title)

        for i in range(len(axs)):
            axs[i].set_xlabel("Year")
            axs[i].set_ylabel("Deformation [mm]")
            axs[i].set_title(titles[i])
            axs[i].grid(True, color="lightgrey")
            axs[i].set_axisbelow(True)
            axs[i].plot(self.my_data.data.columns, self.my_data.data.iloc[self.NNindices[self.counter][i], :].T * 1000,
                        '-o', markersize=2, linewidth=1, color='grey', markerfacecolor='C0', markeredgecolor='C0')
            axs[i].scatter(self.my_data.data.columns,
                           self.my_data.data.iloc[self.NNindices[self.counter][i], :].T * 1000 + self.my_data.pi_shift,
                           s=5, c="wheat")
            axs[i].scatter(self.my_data.data.columns,
                           self.my_data.data.iloc[self.NNindices[self.counter][i], :].T * 1000 - self.my_data.pi_shift,
                           s=5, c="lightblue")

            if self.my_data.data.iloc[self.NNindices[self.counter][i], :].max() * 1000 < 5:
                axs[i].set_ylim(top=25)
            else:
                axs[i].set_ylim(top=self.my_data.data.iloc[self.NNindices[self.counter][i], :].max() * 1000 + 20)
            if self.my_data.data.iloc[self.NNindices[self.counter][i], :].min() * 1000 > -10:
                axs[i].set_ylim(bottom=-40)
            else:
                axs[i].set_ylim(bottom=self.my_data.data.iloc[self.NNindices[self.counter][i], :].min() * 1000 - 20)

            # plotting the fitted model
            axms[i].axhline(y=0, xmin=0, xmax=1, c='black', lw=1)
            x = np.linspace(0, self.end_x, int(self.my_data.num_of_obs_full))
            #         print(self.model.get())
            eval('axms[i].plot(x,1000*(' + self.my_data.fitted_models[self.NNindices[self.counter][i]] +
                 '), color="blueviolet")')
            axms[i].tick_params(axis='x', top=False, labeltop=False)

        axs[0].set_title(titles[0], color="darkgreen")

    def MiniMap(self):

        top = Toplevel(self.topNN)
        top.rowconfigure(0, weight=1)
        top.rowconfigure(1, weight=10)
        top.rowconfigure(2, weight=1)
        top.columnconfigure(0, weight=1)
        top.configure(background="white")

        title2 = Label(top, text='Relative location for 5 closest spatial neighbours of the point #' + str(self.counter)
                                + ':', font="Verdana 12", bg='white')
        title2.grid(row=0, column=0, sticky=N + S, pady=5)
        title2.grid_columnconfigure(0, weight=1)

        mapFrame = Frame(top)
        mapFrame.grid(row=1, column=0, sticky=N + S + W + E)
        mapFrame.grid_columnconfigure(0, weight=1)

        figMap = Figure()
        axMap = figMap.add_subplot(111)
        # figMap.set_tight_layout(True)

        xs = self.my_data.loc_m[self.NNindices[self.counter], 0] - self.my_data.loc_m[self.NNindices[self.counter][0], 0]
        ys = self.my_data.loc_m[self.NNindices[self.counter], 1] - self.my_data.loc_m[self.NNindices[self.counter][0], 1]
        text_labels = [str(ind) for ind in self.NNindices[self.counter]]

        axMap.grid(True, color='lightgrey')
        axMap.set_axisbelow(True)
        axMap.axhline(ls='--', color='grey', zorder=3)
        axMap.axvline(ls='--', color='grey', zorder=3)
        axMap.scatter(xs[0], ys[0], zorder=4, color='C0')
        axMap.scatter(xs[1:], ys[1:], zorder=4, color='C1')
        axMap.set_xlabel("x [m]")
        axMap.set_ylabel("y [m]")

        range_x = max(xs) - min(xs)
        range_y = max(ys) - min(ys)

        for i in range(0, len(self.NNindices[self.counter])):
            axMap.text(xs[i] + 0.01*range_x, ys[i] + 0.01*range_y, text_labels[i])

        canvasMap = FigureCanvasTkAgg(figMap, mapFrame)
        canvasMap.draw()
        canvasMap.get_tk_widget().pack(side=TOP, fill=BOTH, expand=True)

        closeButton = ttk.Button(top, text='Close', command=top.destroy, style="My.TButton")
        closeButton.grid(row=2, column=0, sticky=S, pady=[3, 10])
        closeButton.grid_columnconfigure(0, weight=1)

    def NNPlotsBox(self):
    
        top = Toplevel(self.master)
        self.topNN = top
        top.rowconfigure(0, weight=1)
        top.rowconfigure(1, weight=10)
        top.rowconfigure(2, weight=1)
        top.columnconfigure(0, weight=1)
    
        w, h = self.master.winfo_screenwidth(), self.master.winfo_screenheight()
        top.geometry("%dx%d+0+0" % (0.9*w, 0.9*h))
    
        top.configure(background="white")
    
        title1 = Label(top, text='Time series for 5 closest spatial neighbours of the point #' + str(self.counter) +
                                 ':', font="Verdana 12", bg='white')
        title1.grid(row=0, column=0, sticky=N, ipady=5)
        title1.grid_columnconfigure(0, weight=1)
    
        mapButton = ttk.Button(top, text='Show map', command=self.MiniMap, style="My.TButton")
        mapButton.grid(row=0, column=0, sticky=E, padx=25)
        mapButton.grid_columnconfigure(0, weight=1)
    
        plotFrame = Frame(top)
        plotFrame.grid(row=1, column=0, sticky=N + S + W + E)
        plotFrame.grid_columnconfigure(0, weight=1)
    
        self.figSubs = Figure()
        self.ax_1 = self.figSubs.add_subplot(232)
        self.axm_1 = self.ax_1.twiny()
        self.ax_2 = self.figSubs.add_subplot(231)
        self.axm_2 = self.ax_2.twiny()
        self.ax_3 = self.figSubs.add_subplot(233)
        self.axm_3 = self.ax_3.twiny()
        self.ax_4 = self.figSubs.add_subplot(234)
        self.axm_4 = self.ax_4.twiny()
        self.ax_5 = self.figSubs.add_subplot(235)
        self.axm_5 = self.ax_5.twiny()
        self.ax_6 = self.figSubs.add_subplot(236)
        self.axm_6 = self.ax_6.twiny()
        self.figSubs.set_tight_layout(True)
    
        self.MakeSubPlots()
    
        canvasSubs = FigureCanvasTkAgg(self.figSubs, plotFrame)
        canvasSubs.draw()
        canvasSubs.get_tk_widget().pack(side=TOP, fill=BOTH, expand=True)
    
        closeButton = ttk.Button(top, text='Close', command=top.destroy, style="My.TButton")
        closeButton.grid(row=2, column=0, sticky=S, pady=[0,15])
        closeButton.grid_columnconfigure(0, weight=1)

    def AcceptButton(self):

        print("Adding Accept to the answers list under", self.counter)
        self.SaveAnswer('ok')

        if (self.checked.get()):
            if (self.checked_rand.get()):
                self.counterPrev = self.counter
                self.counter = randint(0, (self.my_data.data.shape[0]))
            else:
                # change the counter (+1)
                self.counterPrev = self.counter
                self.counter += 1
            print("Going to", self.counter)
            self.PointUpdate()

    def UnwrappingButton(self):

        print("Adding 'unwrapping error' to the answers list under", self.counter)
        self.SaveAnswer('U')

        if (self.checked.get()):
            if (self.checked_rand.get()):
                self.counterPrev = self.counter
                self.counter = randint(0, (self.my_data.data.shape[0]))
            else:
                # change the counter (+1)
                self.counterPrev = self.counter
                self.counter += 1
            print("Going to", self.counter)
            self.PointUpdate()

    def ModelButton(self):

        print("Adding 'model error' to the answers list under", self.counter)
        self.SaveAnswer('M')

        if (self.checked.get()):
            if (self.checked_rand.get()):
                self.counterPrev = self.counter
                self.counter = randint(0, (self.my_data.data.shape[0]))
            else:
                # change the counter (+1)
                self.counterPrev = self.counter
                self.counter += 1
            print("Going to", self.counter)
            self.PointUpdate()

    def BothButton(self):

        print("Adding 'unwrapping and model error' to the answers list under", self.counter)
        self.SaveAnswer('B')

        if (self.checked.get()):
            if (self.checked_rand.get()):
                self.counterPrev = self.counter
                self.counter = randint(0, (self.my_data.data.shape[0]))
            else:
                # change the counter (+1)
                self.counterPrev = self.counter
                self.counter += 1
            print("Going to", self.counter)
            self.PointUpdate()

    def SaveAnswer(self, answer):
        if str(self.my_data.point_id.iloc[self.counter]) in self.my_data.answers.keys():
            self.my_data.answers[str(self.my_data.point_id.iloc[self.counter])][1] = answer
        else:
            self.my_data.answers[str(self.my_data.point_id.iloc[self.counter])] = [None] * 3
            self.my_data.answers[str(self.my_data.point_id.iloc[self.counter])][0] = self.counter
            self.my_data.answers[str(self.my_data.point_id.iloc[self.counter])][1] = answer

    def SaveComment(self, comment):
        if str(self.my_data.point_id.iloc[self.counter]) in self.my_data.answers.keys():
            self.my_data.answers[str(self.my_data.point_id.iloc[self.counter])][2] = comment
        else:
            self.my_data.answers[str(self.my_data.point_id.iloc[self.counter])] = [None] * 3
            self.my_data.answers[str(self.my_data.point_id.iloc[self.counter])][0] = self.counter
            self.my_data.answers[str(self.my_data.point_id.iloc[self.counter])][2] = comment

    def NextButton(self):

        if (self.checked_rand.get()):
            self.counterPrev = self.counter
            self.counter = randint(0, (self.my_data.data.shape[0])) 
        else:
            # change the counter (+1)
            self.counterPrev = self.counter
            self.counter += 1
        print("Going to", self.counter)
        self.PointUpdate()

    def BackButton(self):
        
        self.counterPrev = self.counter
        self.counter -= 1
        print("Going to", self.counter)
        self.PointUpdate()
        
    def PreviousButton(self):
        
        tmp = self.counter
        self.counter = self.counterPrev
        self.counterPrev = tmp
        print("Going to", self.counter)
        self.PointUpdate()

    def FinishButton(self):
        
        # convert the segmentation lines dict to the one with point_id keys
        segment = {}
        points = {}
        for key in self.segmentation.keys():
            segment[str(self.my_data.point_id.iloc[key])] = list(set([item[1] for item in self.segmentation[key]]))
        # print(segment)
        for key in self.points.keys():
            points[str(self.my_data.point_id.iloc[key])] = list(set([k[2] for k in self.points[key]]))
        # print(points)
        
        # create a very nice dict to be exported
        full_answers = {}
        for key, value in self.my_data.answers.items():
            full_answers[key] = {"ts_num": value[0],
                                    "answer": value[1],
                                    "comment": value[2]}
            if key in segment.keys():
                full_answers[key]["segm_lines"] = segment[key]
            else:
                full_answers[key]["segm_lines"] = []
            if key in points.keys():
                full_answers[key]["points"] = points[key]
            else:
                full_answers[key]["points"] = []
        for key in segment.keys():
            if key not in full_answers.keys():
                full_answers[key] = {"ts_num": self.my_data.point_id[self.my_data.point_id == key].index[0],
                                    "answer": [],
                                    "comment": [],
                                    "segm_lines": segment[key]}
                if key in points.keys():
                    full_answers[key]["points"] = points[key]
                else:
                    full_answers[key]["points"] = []
        for key in points.keys():
            if key not in full_answers.keys():
                full_answers[key] = {"ts_num": self.my_data.point_id[self.my_data.point_id == key].index[0],
                                    "answer": [],
                                    "comment": [],
                                    "segm_lines": [],
                                    "points": points[key]}
        # print(full_answers)

        # Finishing and saving the answers to the text file:
        date = datetime.datetime.today().strftime('%Y-%m-%d-%H-%M')
                
        directory = os.path.join(os.getcwd(),'answers')
        if not os.path.exists(directory):
            os.makedirs(directory)
        
        ans_file = open(os.path.join(directory,"answers_" + str(date) + ".txt"), "w")
        ans_file.write("You finished at the time-series number: %d\n" % self.counter)
        ans_file.write("The answers file for the dataset %s: \n" % self.datafile)
        
        for key in full_answers.keys():
            ans_file.write(str(key)+":"+ str(full_answers[key])+"\n")
                
        tkinter.messagebox.showinfo("Thanks!", "Thank you very much for your help!\n\n"
                                               "You finished at the time-series #" + str(self.counter)
                                    + "!\n\nYour answers have been saved to a textfile. If you ever want to do this again,"
                                      " you can check the number in your last answers file and "
                                      "you can start where you finished by using 'Go to...' :)")

        print("Saving answers to " + "answers_" + str(date) + ".txt")
        # Closing the window and the program
        self.close_all()

    def ChangeButton(self):
        
        new_count = simpledialog.askinteger('Go to...', 'Choose to which time-series you want to go\n'
                                            +'(a number between 0 and ' + str(self.my_data.data.shape[0] - 1) + '):',
                                            minvalue=0, maxvalue=self.my_data.data.shape[0], parent=self.master)
        # print(new_count)
        if new_count is not None:
            self.counterPrev = self.counter
            self.counter = new_count
            print("Going to", self.counter)
            self.PointUpdate()
            
    def ClearButton(self):
        
        self.segmentation.pop(self.counter, None)
        
        # change the plot
        self.ax.clear()
        self.ax2.clear()
        self.MakeAPlot()
        self.canvas.draw()
        
    def ClearPointsButton(self):
        
        self.points.pop(self.counter, None)
        self.shift.pop(self.counter, None)
        
        # change the plot
        self.ax.clear()
        self.ax2.clear()
        self.MakeAPlot()
        self.canvas.draw()

        
    def AddComment(self):

        inputDialog = DialogBox(self.master)
        self.master.wait_window(inputDialog.top)
        try:
            print('Comment: ', inputDialog.comment)
        except AttributeError:
            pass
        self.SaveComment(inputDialog.comment)
        
    
    def PointUpdate(self):

        # change the title label
        self.title_var.set("Time-series correctness assessment\n" + self.my_data.area_name + " - time-series #" + str(self.counter))

        # change the point info
        self.point_info.set(str(self.my_data.point_id.iloc[self.counter]) + "\n"
                            + "%0.1f" % (self.my_data.linear.iloc[self.counter] * 1000) + "\n"
                            + "%0.5f" % (self.my_data.height.iloc[self.counter]) + "\n"
                            + "%0.6f" % (self.my_data.dem_height.iloc[self.counter]) + "\n"
                            + str(self.my_data.data.shape[1]))

        # change the fitted model
        if self.my_data.fitted_models:
            self.model.set(self.my_data.fitted_models[self.counter])

        # change the plot
        self.ax.clear()
        self.ax2.clear()
        self.MakeAPlot()
        self.canvas.draw()
        

    def OnClick(self, event):
        
        if self.labelmode.get() == "lines":
            lims = self.ax.get_ylim()
            if event.button == 1:
                clicked_x, clicked_y = event.inaxes.transData.inverted().transform((event.x, event.y))
                # print(event.x, event.y)
                # print('x= ',clicked_x ,'y= ',clicked_y)

                if 0 < clicked_x < self.end_x and lims[0] < clicked_y < lims[1]:

                    xy_disp = event.inaxes.transData.transform(np.vstack([self.x_orig, self.y_orig]).T)
                    x_disp, y_disp = xy_disp.T

                    x_1 = np.abs(x_disp - event.x)

                    closest_1 = np.argmin(x_1[x_disp < event.x])
                    # print(closest_1)

                    if self.counter in self.segmentation.keys():
                        self.segmentation[self.counter].append([clicked_x, closest_1])
                    else:
                        self.segmentation[self.counter] = [[clicked_x, closest_1]]

            if event.button == 3:
                clicked_x, clicked_y = event.inaxes.transData.inverted().transform((event.x, event.y))
                if 0 < clicked_x < self.end_x and lims[0] < clicked_y < lims[1]:
                        if self.counter in self.segmentation.keys():
                            diff = [0,100]
                            for i, item in enumerate(self.segmentation[self.counter]):
                                if np.abs(item[0] - clicked_x) < diff[1]:
                                    diff = [i, np.abs(item[0] - clicked_x)]

                            self.segmentation[self.counter].remove(self.segmentation[self.counter][diff[0]])
        
        if self.labelmode.get() == "shift":
            
            lims = self.ax.get_ylim()
            starter = 1 - (self.shift_counter % 2) 
            if event.button == 1:
                if starter == 0:
                    clicked_x, clicked_y = event.inaxes.transData.inverted().transform((event.x, event.y))
                    # print(event.x, event.y)
                    # print('x= ', clicked_x , 'y= ', clicked_y)
                    if 0 < clicked_x < self.end_x and lims[0] < clicked_y < lims[1]:

                        xy_disp = event.inaxes.transData.transform(np.vstack([self.x_orig, self.y_orig]).T)
                        x_disp, y_disp = xy_disp.T

                        x_1 = np.abs(x_disp - event.x)
                        y_1 = np.abs(y_disp - event.y)
                        dist_1 = np.sqrt(np.square(x_1) + np.square(y_1))
                        closest_1 = np.argmin(dist_1)
                        print(closest_1)

                        if self.counter in self.shift.keys():
                            self.shift[self.counter].append([clicked_x, clicked_y, closest_1, starter])
                        else:
                            self.shift[self.counter] = [[clicked_x, clicked_y, closest_1, starter]]

                        self.shift_counter += 1

                if starter == 1:
                    clicked_x, clicked_y = event.inaxes.transData.inverted().transform((event.x, event.y))
                    # print(event.x, event.y)
                    # print('x= ',clicked_x ,'y= ',clicked_y)
                    if 0 < clicked_x < self.end_x and lims[0] < clicked_y < lims[1]:

                        xy_disp = event.inaxes.transData.transform(np.vstack([self.x_orig, self.y_orig]).T)
                        x_disp, y_disp = xy_disp.T

                        x_1 = np.abs(x_disp - event.x)
                        y_1 = np.abs(y_disp - event.y)
                        dist_1 = np.sqrt(np.square(x_1) +np.square(y_1))
                        closest_1 = np.argmin(dist_1)
                        # print(closest_1)

                        if self.counter in self.shift.keys():
                            self.shift[self.counter].append([clicked_x, clicked_y, closest_1, starter])
                        else:
                            self.shift[self.counter] = [[clicked_x, clicked_y, closest_1, starter]]

                        self.shift_counter += 1

                        ind_old = self.shift[self.counter][-2][2]
                        if ind_old < closest_1:
                            for i in range(ind_old, closest_1+1):
                                if self.counter in self.points.keys():
                                    self.points[self.counter].append([self.x_orig[i], self.y_orig[i], i])
                                else:
                                    self.points[self.counter] = [[self.x_orig[i], self.y_orig[i], i]]

                        if ind_old > closest_1:
                            for i in range(closest_1, ind_old+1):
                                if self.counter in self.points.keys():
                                    self.points[self.counter].append([self.x_orig[i], self.y_orig[i], i])
                                else:
                                    self.points[self.counter] = [[self.x_orig[i], self.y_orig[i], i]]

        if self.labelmode.get() == "points":
            
            lims = self.ax.get_ylim()
            print(lims)

            if event.button == 1:
                clicked_x, clicked_y = event.inaxes.transData.inverted().transform((event.x, event.y))
                # print(self.end_x, self.x_orig, self.y_orig)
                print(event.x, event.y)
                print('x= ',clicked_x ,'y= ',clicked_y)
                if 0 < clicked_x < self.end_x and lims[0] < clicked_y < lims[1]:

                    xy_disp = event.inaxes.transData.transform(np.vstack([self.x_orig, self.y_orig]).T)
                    x_disp, y_disp = xy_disp.T

                    x_1 = np.abs(x_disp - event.x)
                    y_1 = np.abs(y_disp - event.y)
                    dist_1 = np.sqrt(np.square(x_1) + np.square(y_1))
                    closest_1 = np.argmin(dist_1)
                    print(closest_1)

                    if self.counter in self.points.keys():
                        self.points[self.counter].append([clicked_x, clicked_y, closest_1])
                    else:
                        self.points[self.counter] = [[clicked_x, clicked_y, closest_1]]

            if event.button == 3:
                clicked_x, clicked_y = event.inaxes.transData.inverted().transform((event.x, event.y))

                if 0 < clicked_x < self.end_x and lims[0] < clicked_y < lims[1]:
                        if self.counter in self.points.keys():
                            diff = [0,10000]
                            for i, item in enumerate(self.points[self.counter]):
                                xy_disp = event.inaxes.transData.transform([self.x_orig[item[2]], self.y_orig[item[2]]])
                                x_disp, y_disp = xy_disp.T

                                if np.sqrt((x_disp - event.x)**2 + (y_disp- event.y)**2) < diff[1]:
                                    diff = [i, np.sqrt((x_disp - event.x)**2 + (y_disp- event.y)**2)]

                            self.points[self.counter].remove(self.points[self.counter][diff[0]]) 

        # change the plot
        self.ax.clear()
        self.ax2.clear()
        self.MakeAPlot()
        self.canvas.draw()    
        
    def close_all(self):
        
        self.master.quit()
        self.master.destroy()
        
        

        
class DialogBox:

    def __init__(self, parent):
        top = self.top = Toplevel(parent)
        self.Label = Label(top, text='Please give your comment below:', font="Verdana 10")
        self.Label.pack(ipady=5)
        self.EntryBox = Text(top, width=60, height=10)
        self.EntryBox.pack(pady=5, padx=10)

        sd = ttk.Style(master=parent)
        sd.configure("smaller.TButton_smaller", font="TkDefaultFont 10")
        self.SubmitButton = ttk.Button(top, text='Submit', command=self.send, style="smaller.TButton")
        self.SubmitButton.pack(pady=5)

    def send(self):
        self.comment = self.EntryBox.get(1.0, "end-1c")
        self.top.destroy()