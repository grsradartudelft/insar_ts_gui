from tkinter import *
from tkinter import messagebox
from data_acquisition import DataReader
from mywindow import MyWindow

                    
def on_closing():

    if messagebox.askyesno("Quit without saving", "Are you sure you want to quit?\n" \
                              "Your answers won't be saved!\n\n" \
                              "To save use Finish button!\n\n" \
                              "Quit without saving?"):
        root.quit()
        root.destroy()    


# ***** MAIN *****
datafile_amsterdam = "amsterdam_asc_tsx_t116_ps_hoge.csv"
datafile_limburg = "limburg_asc_correction_v5a_ds_abroad.csv"
datafile_s1 = "S1 asc t88.csv"


data_reader_params = {"datafile": datafile_s1,
                      "pi_shift": 18.1,
                      "revisit": 6,
                      "area_name": "Amsterdam",
                      "col_with_model": "graph_000001"}

data_reader = DataReader(data_reader_params)
data = data_reader.read()

root = Tk()
w, h = root.winfo_screenwidth(), root.winfo_screenheight()
root.geometry("%dx%d+0+0" % (int(0.9*w), int(0.9*h)))
wind = MyWindow(root, data)
root.wm_protocol("WM_DELETE_WINDOW", on_closing)
root.mainloop()
