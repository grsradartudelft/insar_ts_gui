import ast

def ReadCorrections(filename):

    corrections_dict = {}
    with open(filename) as fin:
        for line in fin:
            row = ast.literal_eval(line.rstrip('\n,'))
            corrections_dict[row[0]] = {'corr_idx': row[1], 'sign': row[2]}

    return corrections_dict
