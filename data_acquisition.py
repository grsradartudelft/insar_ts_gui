import pandas as pd
from datetime import datetime


class InsarDataset:

    def __init__(self, data_dict):
        self.pi_shift = data_dict["pi_shift"]
        self.revisit = data_dict["revisit"]
        self.area_name = data_dict["area_name"]
        self.data_filename = data_dict["datafile"]
        self.num_of_obs_full = data_dict["num_of_obs_full"]
        self.num_of_act_obs = data_dict["num_of_act_obs"]
        self.point_id = data_dict["point_id"]
        self.loc_m = data_dict["loc_m"]
        self.linear = data_dict["linear"]
        self.quality = data_dict["quality"]
        self.height = data_dict["height"]
        self.dem_height = data_dict["dem_height"]
        self.data = data_dict["data"]
        self.fitted_models = data_dict["fitted_models"]
        self.answers = {}


class DataReader:

    def __init__(self, params):
        self.pi_shift = params["pi_shift"]
        self.revisit = params["revisit"]
        self.area_name = params["area_name"]
        self.data_filename = params["datafile"]
        self.col_with_model = params["col_with_model"]

    def read(self):
        df_loaded = self._load_csv()
        full_data, data_df = self._get_obs_data_df(df_loaded)
        all_obs_num, act_obs_num = self._get_acqs_nums(data_df)
        other_params = self._get_other_params(full_data)
        fitted_models = self._get_fitted_models(full_data)

        data_dict = {"pi_shift": self.pi_shift,
                     "revisit": self.revisit,
                     "area_name": self.area_name,
                     "datafile": self.data_filename,
                     "num_of_obs_full": all_obs_num,
                     "num_of_act_obs": act_obs_num,
                     "data": data_df,
                     "fitted_models": fitted_models,
                     **other_params,
                     }

        dataset = InsarDataset(data_dict)
        return dataset

    def _load_csv(self):
        with open(self.data_filename, "r") as file:
            first_row = file.readline()
        if ";" in first_row:
            sep = ";"
        else:
            sep = ","

        with open(self.data_filename, "r") as file:
            df = pd.read_csv(file, sep=sep)

        return df

    def _get_obs_data_df(self, df):
        dates = []
        for col in df.columns:
            if col.startswith("d_"):
                date_col = datetime.strptime(col[2:], '%Y%m%d').date()
                dates.append(date_col)
                df.rename(columns={col: date_col}, inplace=True)

        return df, df[dates]

    def _get_acqs_nums(self, data_df):
        delta = data_df.columns[-1] - data_df.columns[0]
        all_days = delta.days / self.revisit

        all_meas_num = all_days + 1
        act_meas_num = len(data_df.columns)

        return all_meas_num, act_meas_num

    def _get_other_params(self, full_df):
        return {"point_id": full_df["pnt_id"],
                "loc_m": [full_df["pnt_rdx"].values, full_df["pnt_rdy"].values] if ("pnt_rdx" in full_df.columns
                                                                      and "pnt_rdy" in full_df.columns) else [],
                "linear": full_df["pnt_linear"],
                "quality": full_df["pnt_quality"],
                "height": full_df["pnt_height"],
                "dem_height": full_df["pnt_demheight"]}

    def _get_fitted_models(self, df):
        if self.col_with_model and self.col_with_model in df.columns:
            fitted = df[self.col_with_model].copy()
            fitted_trim = [fitted[i][0:-23] for i in range(0, fitted.size)]

            for i in range(0, fitted.size):
                tmp1 = fitted_trim[i].replace("Math", "np")
                tmp2 = tmp1.replace("pow", "power")
                tmp3 = tmp2.replace("&", "and")
                fitted_trim[i] = tmp3.replace("PI", "pi")
            return fitted_trim
        return []
