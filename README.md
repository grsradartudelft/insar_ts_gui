# InSAR time-series assessment GUI 

The InSAR time-series assessment GUI aims to provide a tool to interactively assess whether the fitted displacement model to an InSAR time-series is 'correct' or whether there are any phase unwrapping errors. The outcomes are meant to train Machine Learning approaches. 

The application works on Windows (checked for Win10) and on Linux (checked for Ubuntu 16.04). The script needs the following modules: _matplotlib_, _numpy_, _pandas_ (also given in the requirements.txt), so if you have Anaconda installed, then it should be fine.

#### User instructions
After you run the notebook the GUI will show you time-series for points in Amsterdam. Your task is to assess whether you think that the fitted model is correct or not. Are there any visible errors, such as unwrapping errors or the model doesn't account for the periodic behavior (or it does where it shouldn't), etc. You don't have to specify why, if you think the model could be better, just click "Don't Accept" button.

The figure shows the main measurement (blue) and the model fitted to it. Additionally, two parallel time-series are plotted to represent $\pm\ 2\pi$ for the measurement, so that it would be easier to spot a potential unwrapping error.

Based on your expert knowledge, decide whether you accept the time-series (then click __It's fine__) or not, i.e., you find something that should be considered but wasn't, unwrapping errors, steps and discontinuities, decorrelation after some point, the plot just looks wrong for you, etc. (then click __Unwrapping__, __Model__, __Both__, accordingly).

You can check the "Go to the next one automatically" box and the new time-series will appear after you click one of the four answer buttons. You don't have to answer to all time-series, if you want to skip one, just click __Next__. Also if you would like to change you mind, then click __Back__ and choose again. To go to some specific time-series click __Go to...__ and insert a number. You can also check the "Shuffle points randomly" and view the points in random order.

In summary, __Next__ goes to a next time-series (+1), __Back__ to a one before (-1) and __Previous__ goes one back so it works if you used __Go to...__ or you were viewing points randomly. (Example: you are on \#17 and go randomly to \#4338, __Back__ will take you to \#4337 and __Previous__ to \#17.)

You can also mark specific points as unwrapping errors, draw segmentation lines, and mark whole segments as unwrapping shifts. To choose an option check a corresponding box at the bottom of the window:

***Segmentation*** - click on the plot (no matter the y-axis position, so anywhere along the date you would like to draw a line) and a black dashed line will be drawn. You can delete it with a right mouse button click (deletes the closest one to where you click). You can also delete all of them at once by clicking on __Clear segmentation__ button.

***Points*** - click on a measurement point (or close to it) and it will be marked and change its colour to red. Click with a right button to delete the marking on a closest point. To clear everything click __Clear points__.

***Shift*** - click on a point from which the whole shift starts (it'll change to red and a vertical line will appear), then click on the last point in the shift. It will cause all the points between to be marked as unwrapping errors (exactly like using *Points* option). You can delete single points just as with *Points*. To clear everything click __Clear points__.

You can also have a look at how the time series for 5 nearest neighbours (spatially) look like. To open a window with additional plots click on __Show neighbours__. You will see six plots, where top middle one is the current time series (main one) and five around are the nearest neighbours. In the title you can see the number of a specific neighbour and its distance to the main one. If you click __Show map__ in the top right corner, you will see a minimap with the relative location of all 5 neighbours with respect to the main one.


### Main window:
![Guide image](guide_main.PNG)


### Nearest neighbours window:
![Guide image](guide_sub.PNG)


________________________
Your answers will be saved in a .txt file in a folder named *answers* inside the folder where this script is. The name of the file includes the date and time so you don't have to worry about overwriting in case you would like to work on it more than once. 

If you would like to continue working on this task and start from a different point than the first one, then click on "Go to..." and insert the number.

### !! IMPORTANT !!  
__Please, always end the task by clicking "Finish" and not by closing the window with X on the top__ because your answers will only be saved when you finish with "Finish".

